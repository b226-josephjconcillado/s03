-- MySQL CRUD Operations

-- [SECTION] Inserting A record

-- SYNTAX: INSERT INTO table_name (column_nameA) VALUES (ValueA);

INSERT INTO artists (name) VALUES("Psy");
INSERT INTO artists (name) VALUES("Rivermaya");

SELECT * FROM artists;

INSERT INTO albums (album_title, date_release, artist_id) VALUES("Psy 6", "2012-07-15",1);
INSERT INTO albums (album_title, date_release, artist_id) VALUES("Trip", "1996-02-14",2);
INSERT INTO albums (album_title, date_release, artist_id) VALUES("Kundiman", "2008-01-21",2);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style",339,"K-Pop",1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman",524,"OPM",2);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata",441,"OPM",2);

SELECT * FROM table_name;
SELECT column_nameA, column_nameB FROM table_name;

SELECT song_name, genre FROM songs;

SELECT album_title, date_release FROM albums;

-- based on specific value of the tables 
-- "WHERE" clause is used to filter records and to extract only those records that fullfil a specific condition.

SELECT song_name,genre FROM songs WHERE genre = "OPM";

-- display the title and length of the OPM songs that are more than 4 minutes 30 secs.

SELECT song_name,genre FROM songs WHERE genre = "OPM" AND length > 430;


-- Updating Records
-- Syntax:
    -- UPDATE table_name SET column_nameA = updated_valueA WHERE condition;

UPDATE songs SET length = 424 WHERE song_name = "Kundiman";

-- Deleting Records
-- Syntax:
    DELETE FROM table_name WHERE condition;

DELETE FROM songs WHERE length > 430 AND genre = "OPM";